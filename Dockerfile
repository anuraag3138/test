FROM microsoft/dotnet:sdk AS build-env

WORKDIR /app
USER root

COPY . ./

RUN chown -R 1001 ./ && \
    chgrp -R 0 ./ && \
    chmod -R 777 ./
USER 1001

COPY *.csproj ./
RUN dotnet restore

RUN dotnet publish -c Release -o out

FROM microsoft/dotnet:aspnetcore-runtime

WORKDIR /app
COPY --from=build-env /app/out .
ENTRYPOINT ["dotnet", "anurag.dll"]
